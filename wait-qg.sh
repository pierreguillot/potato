#!/bin/bash
attempt_counter=0
max_attempts=3
url='https://sonarcloud.io/api/qualitygates/project_status?projectKey=potato-pierre&pullRequest='$1
echo 'fetching url ' $url
until $(curl -sS $url | grep -q -e '"projectStatus":{"status":"OK"'; ); do
  printf '.'
    if [ ${attempt_counter} -eq ${max_attempts} ];then
      echo "Max attempts reached, exit with error code 1"
      exit 1
    fi
  attempt_counter=$(($attempt_counter+1))
  sleep 6
done
printf 'status OK\n'
